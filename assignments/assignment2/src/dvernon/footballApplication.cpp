/* 

   Football
   --------
   Application file ... refer to the interface file (football.h) for details of the functionality of this program

                                                    
   Author
   ------

   David Vernon, CMU-R
   1 February 2017


   Audit Trail
   -----------

   - Removed core functionality (specifically the sorting) to allow the input, data parsing, and output code
     to be given to the students as an example of string handling and the use of structures in C
     DV 21/01/2017

*/

 
#include "football.h"
 
int main() {

   int debug = TRUE;
   int number_of_tournaments, number_of_teams, number_of_games;
   char team1_name[TEAM_NAME];
   char team2_name[TEAM_NAME];
   char tournament_name[TOURNAMENT_NAME];
   char result[RESULT_SIZE];
   int tournament_number;
   int team_number;
   int game_number;
   int team1_score;
   int team2_score;
   int sort_key;

   struct team_type team[MAX_TEAMS];

   FILE *fp_in, *fp_out;

   if ((fp_in = fopen("../data/input.txt","r")) == 0) {
	  printf("Error can't open input in.txt\n");
      exit(0);
   }

   if ((fp_out = fopen("../data/output.txt","w")) == 0) {
	  printf("Error can't open output out.txt\n");
      exit(0);
   }

   // read the number of tournaments

   fscanf(fp_in, "%d", &number_of_tournaments);
   fgetc(fp_in); // move past end of line for subsequent fgets

   if (debug) printf ("%d\n",number_of_tournaments);

   for (tournament_number = 0; tournament_number < number_of_tournaments; tournament_number++) {

      // get the tournament name

      fgets(tournament_name, TOURNAMENT_NAME, fp_in);
      remove_new_line(tournament_name);

      if (debug) printf ("%s\n",tournament_name);

      // get the number of teams

      fscanf(fp_in, "%d", &number_of_teams);

      fgetc(fp_in); // move past end of line for subsequent fgets

      if (debug) printf ("%d\n",number_of_teams);

      // get each team name and add it to the array of teams

      for (team_number=0; team_number < number_of_teams; team_number++) {  
         fgets(team1_name, TEAM_NAME, fp_in);

         remove_new_line(team1_name);

         if (debug) printf ("%s\n",team1_name);

		 strcpy(team[team_number].name, team1_name);
      }

      // now initialize the team data

      initialize_team_data(team, number_of_teams);

      // get the number of games

      fscanf(fp_in, "%d", &number_of_games);
      fgetc(fp_in); // move past end of line for subsequent fgets

      if (debug) printf ("%d\n",number_of_games);

      // process each game in turn, computing the five team statistics
      // (points, wins, goal_difference, goals, games_playes) 

      for (game_number=0; game_number < number_of_games; game_number++) { 
         
         fgets(result, RESULT_SIZE, fp_in);

         remove_new_line(result);

         // now parse the results to extract the scores for each team

         parse_result(result,team1_name,&team1_score,team2_name, &team2_score);

         if (debug) printf ("%30s %d %30s %d\n",team1_name,team1_score,team2_name,team2_score);

         // update the team data

         update_team_data(team, number_of_teams, team1_name,team1_score,team2_name,team2_score);
      }

      // sort for all six keys, least important first

      // The function call has been removed from here (i.e. the application file)
      // The function definition has been removed from the implementation file
      // The function declaration has been removed from the interface file
      
      // The results are unsorted ...

      print_results(team, number_of_teams, tournament_name);
      print_results_to_file(fp_out,team, number_of_teams, tournament_name);
   }
    
   fclose(fp_in);
   fclose(fp_out);
}

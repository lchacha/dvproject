/* 
   Football
   --------
   Implementation file ... refer to the interface file (football.h) for details of the functionality of this program

                                                    
   Author
   ------

   David Vernon, CMU-R
   1 February 2017


   Audit Trail
   -----------

   - Removed core functionality (specifically the sorting) to allow the input, data parsing, and output code
     to be given to the students as an example of string handling and the use of structures in C
     DV 21/01/2017

*/
 
#include "football.h"


// Extract team names and scores from a string of the format 
//
// Team E#2@1#Team C
//
// to yield
//
// team1:       Team E
// team1_score: 2
// team2:       Team C
// team2_score: 1

void parse_result(char result[RESULT_SIZE], char team1[], int *team1_score, char team2[], int *team2_score) {

   int i, j;
   char score[5];

   strcpy(team1,"");
   *team1_score = 0;
   strcpy(team2,"");
   *team2_score = 0;

   i = 0;
   j = 0;

   while (result[i] != '#') {
      team1[j] = result[i];
      i++;
      j++;
   }

   team1[j] = '\0';
   i++; // skip over #
   j = 0;

   while (result[i] != '@') {
      score[j] = result[i];
      i++;
      j++;
   }

   score[j] = '\0';

   *team1_score = atoi(score);

   i++; // skip over @

   j = 0;

   while (result[i] != '#') {
      score[j] = result[i];
      i++;
      j++;
   }

   score[j] = '\0';

   *team2_score = atoi(score);

   i++; // skip over #

   j = 0;

   while (result[i] != '\0') {
      team2[j] = result[i];
      i++;
      j++;
   }

   team2[j] = '\0';
}

void remove_new_line(char string[]) {

   int i;

   i=0;

   while(string[i] != '\0' && string[i] != '\n')
      i++;

   string[i] = '\0';
}

void initialize_team_data(struct team_type team[], int number_of_teams) {

   int team_number;

   for (team_number=0; team_number < number_of_teams; team_number++) {  
      team[team_number].games_played = 0;
      team[team_number].goals_scored = 0;
      team[team_number].goals_conceded = 0;
      team[team_number].wins = 0;
      team[team_number].ties = 0;
      team[team_number].points = 0;
   }
}

void update_team_data(struct team_type team[], 
                      int number_of_teams, char team1_name[],
                      int team1_score, char team2_name[], int team2_score) {

   int team_number;

   // find first team

   team_number=0;

   while (strcmp(team1_name, team[team_number].name) != 0) { // dangerous: assumes that the team is in the list
      team_number++;
   }

   team[team_number].games_played++;
   team[team_number].goals_scored += team1_score;
   team[team_number].goals_conceded += team2_score;

   if (team1_score > team2_score) {
      // win
      team[team_number].points += 3;
      team[team_number].wins += 1;
   }
   else if (team1_score == team2_score) {
      // draw
      team[team_number].points += 1;
      team[team_number].ties += 1;
   }

   // find second team
   team_number=0;

   while (strcmp(team2_name, team[team_number].name) != 0) { // dangerous: assumes that the team is in the list
      team_number++;
   }

   team[team_number].games_played++;
   team[team_number].goals_scored += team2_score;
   team[team_number].goals_conceded += team1_score;

   if (team2_score > team1_score) {
      // win
      team[team_number].points += 3;
      team[team_number].wins += 1;
   }
   else if (team2_score == team1_score) {
      // draw
      team[team_number].points += 1;
      team[team_number].ties += 1;
   }
}


void print_results(struct team_type team[], int number_of_teams, char tournament[]) {

   int team_number;

   printf("%s\n", tournament);

   for (team_number=0; team_number < number_of_teams; team_number++) {  
      printf("%d) ",  team_number);
      printf("%s ",   team[team_number].name);  
      printf("%dp, ", team[team_number].points);
      printf("%dg ",  team[team_number].games_played);
      printf("(%d-%d-%d), ", team[team_number].wins, 
                             team[team_number].ties,
                             team[team_number].games_played - team[team_number].wins - team[team_number].ties);
      printf("%dgd ",  team[team_number].goals_scored-team[team_number].goals_conceded);
      printf("(%d-%d)",team[team_number].goals_scored,team[team_number].goals_conceded);
      printf("\n");
   }
   printf("\n");
}

void print_results_to_file(FILE *fp, struct team_type team[], int number_of_teams, char tournament[]) {

   int team_number;

   fprintf(fp,"%s\n", tournament);

   for (team_number=0; team_number < number_of_teams; team_number++) {  
      fprintf(fp,"%d) ",  team_number);
      fprintf(fp,"%s ",   team[team_number].name);  
      fprintf(fp,"%dp, ", team[team_number].points);
      fprintf(fp,"%dg ",  team[team_number].games_played);
      fprintf(fp,"(%d-%d-%d), ", team[team_number].wins, 
                             team[team_number].ties,
                             team[team_number].games_played - team[team_number].wins - team[team_number].ties);
      fprintf(fp,"%dgd ",  team[team_number].goals_scored-team[team_number].goals_conceded);
      fprintf(fp,"(%d-%d)",team[team_number].goals_scored,team[team_number].goals_conceded);
      fprintf(fp,"\n");
   }
   fprintf(fp,"\n");
}
/* 
   football.h - interface file for Assignment 2 - Multi-key Sorting 
   ================================================================

   This program is for course 04-630 Data Structures and Algorithms for Engineers 

   It produces a ranked (i.e. sorted) list of teams in a series of football tournaments. 
   It reads the tournament name, team names, and results of games played from and outputs a sorted list of
   the tournament standings.

   A team wins a game if it scores more goals than its opponent, and loses if it scores fewer goals.  
   Both teams tie if they score the same number of goals. 
   A team earns 3 points for each win, 1 point for each tie, and 0 points for each loss.

   Teams are ranked (i.e. sorted) according to these rules (in this order):

   1.	Most points earned.
   2.	Most wins.
   3.	Most goal difference (i.e., goals scored � goals against).
   4.	Most goals scored.
   5.	Fewest games played.
   6.	Case-insensitive lexicographic order.

 
   Input data with test data is provided in an input file named input.txt.  
   This input file is located in the data directory. 
   Since this directory is a sibling directory of the bin directory where the example .exe file resides, 
   the filename used when opening the file is "../data/input.txt".

   Output for the test case is written to an output file "../data/output.txt"


   Input
   -----

   The first line of input will be an integer N in a line alone (0 < N < 1000). 

   Then follow N tournament descriptions.

   Each tournment description comprises several lines of input, as follows.

   Each tournament description begins with a tournament name on a single line. 
   These names can be any combination of at most 100 letters, digits, spaces, etc.

   The next line will contain a number T (1 < T <= 30), which stands for the number of teams participating in this tournament.

   Then follow T lines, each containing one team name. 
   Team names consist of at most 30 characters, and may contain any character with ASCII code greater than or equal to 32 (space), 
   except for �#� and �@� characters.

   Following the team names, there will be a non-negative integer G on a single line which stands for 
   the number of games already played in this tournament.  G will be no greater than 1000.  
   G lines then follow with the results of games played in the format:

      team_name_1#goals1@goals2#team_name_2

   For example, Team A#3@1#Team B means that in a game between Team A and Team B, Team A scored 3 goals and Team B scored 1.  
   All goals will be non-negative integers less than 20.  
   
   You may assume that all team names in game results will have appeared in the team names section, 
   and that no team will play against itself.


   Output
   ------
 
   The first line of the output file should contain your Andrew Id.

   For each tournament, you must output the tournament name in a single line.  
   In the next T lines you must output the standings, according to the rules above. 
   Should lexicographic order be needed as a tie-breaker, it must be done in a case-insensitive manner.  
   The output format for each line is shown below:

   [a]) Team_name [b]p, [c]g ([d] �[e]-[f]), [g]gd ([h]-[i])

   where [a] is team rank, 
         [b] is the total points earned,  
         [c] is the number of games played, 
         [d] is wins, 
         [e] is ties, 
         [f]  is losses, 
         [g] is goal difference, 
         [h] is goals scored, and 
         [i] is goals against.

   There must be a single blank space between fields and a single blank line between output sets.  
   See the sample output for examples.


   Sample Input
   ------------
   
   2
   World Cup 2020 � Group A
   4
   Brazil
   Norway
   Morocco
   Scotland
   6
   Brazil#2@1#Scotland
   Norway#2@2#Morocco
   Scotland#1@1#Norway
   Brazil#3@0#Morocco
   Morocco#3@0#Scotland
   Brazil#1@2#Norway
   Some strange tournament
   5
   Team A
   Team B
   Team C
   Team D
   Team E
   5
   Team A#1@1#Team B
   Team A#2@2#Team C
   Team A#0@0#Team D
   Team E#2@1#Team C
   Team E#1@2#Team D


   Sample Output
   -------------

   <AndrewId>
   World Cup 1998 � Group A
   1) Brazil 6p, 3g (2-0-1), 3gd (6-3)
   2) Norway 5p, 3g (1-2-0), 1gd (5-4)
   3) Morocco 4p, 3g (1-1-1), 0gd (5-5)
   4) Scotland 1p, 3g (0-1-2),-4gd (2-6)

   Some strange tournament
   1) Team D 4p, 2g (1-1-0), 1gd (2-1)
   2) Team E 3p, 2g (1-0-1), 0gd (3-3)
   3) Team A 3p, 3g (0-3-0), 0gd (3-3)
   4) Team B 1p, 1g (0-1-0), 0gd (1-1)
   5) Team C 1p, 2g (0-1-1), -1gd (3-4)


   Solution Strategy
   -----------------

   The teams are ranked by sorting them with the six criteria (i.e. keys) given above
   1.	Most points earned.
   2.	Most wins.
   3.	Most goal difference (i.e., goals scored � goals against).
   4.	Most goals scored.
   5.	Fewest games played.
   6.	Case-insensitive lexicographic order.

   The sorting is effected with a stable sort to ensure that the order generated when sorted with one criterion (key) is preserved 
   when sorting subsequent criteria.


   File organization
   -----------------

   football.h                  interface file:      contains the declarations required to use the functions that implement 
                                                    the solution to this problem
                                                  
   footballImplementation.cpp  implementation file: contains the definitions of the functions that implement the algorithms 
                                                    used in the implementation
 
   footballApplication.cpp     application file:    contains the code that instantiates the data structures and calls
                                                    the associated functions in order to effect the required functionality 
                                                    for this application

                                                    
   Author
   ------

   David Vernon, CMU-R
   1 February 2017


   Audit Trail
   -----------

   - Removed core functionality (specifically the sorting) to allow the input, data parsing, and output code
     to be given to the students as an example of string handling and the use of structures in C
     DV 21/01/2017

*/
 
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include <ctype.h>

#define TRUE 1
#define FALSE 0
#define MAX_TOURNAMENTS 1000
#define TOURNAMENT_NAME 101
#define MAX_TEAMS 30
#define TEAM_NAME 31
#define MAX_GAMES 1000
#define RESULT_SIZE 2*(TEAM_NAME+4)+3+1

struct team_type {
   char name[TEAM_NAME];
   int points;
   int wins;
   int ties;
   int goals_scored;
   int goals_conceded;
   int games_played;
};

void parse_result(char result[RESULT_SIZE], char team1[], int *team1_score, char team2[], int *team2_score);
void remove_new_line(char string[]);
void initialize_team_data(struct team_type team[], int number_of_teams);
void update_team_data(struct team_type team[], int number_of_teams, char team1_name[],int team1_score, char team2_name[], int team2_score);
void print_results(struct team_type team[], int number_of_teams, char tournament[]);
void print_results_to_file(FILE *fp, struct team_type team[], int number_of_teams, char tournament[]);

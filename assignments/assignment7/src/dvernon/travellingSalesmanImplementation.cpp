/* 

  travellingSalesmanImplementation.cpp - application file for the solution of the travelling saleman problem by exhaustive search using backtracking

  04-630 Data Structures and Algorithms for Engineers Assignment No. 7

  See travellingSalesman.h for details of the problem

  David Vernon
  24 April 2017

*/
 
#include "travellingSalesman.h"

void remove_new_line(char string[]) {
   int i;

   i=0;
   while(string[i] != '\0' && string[i] != '\n')
      i++;
   string[i] = '\0';
}


/* add the backtracking code here */

bool finished = FALSE; /* found all solutions yet? */

void backtrack(int a[], int k, int input) {

   int c[MAXCANDIDATES];     /* candidates for next position  */
   int ncandidates;          /* next position candidate count */
   int i;                    /* counter                       */

   if (is_a_solution(a,k,input)) {
      process_solution(a,k,input);
   } 
   else {
      k = k+1;
      construct_candidates(a,k,input,c,&ncandidates);
      for (i=0; i<ncandidates; i++) {
         a[k] = c[i];
         //make_move(a,k,input);
         backtrack(a,k,input);
         //unmake_move(a,k,input);
        if (finished) return;  /* terminate early */
      }
   }
}

/* Construct all permutations                                                  */

bool is_a_solution(int a[], int k, int n) {
        return (k == n);
}
 
void construct_candidates(int a[], int k, int n, int c[], int *ncandidates) {

        int i;                       /* counter */
        bool in_perm[NMAX];          /* who is in the permutation? */
        for (i=1; i<NMAX; i++) in_perm[i] = FALSE;

        /* original code is incorrect ... a[i(==0)] is not a valid element of the permutation since we start storing them at a[1]

        for (i=0; i<k; i++) in_perm[ a[i] ] = TRUE;

        */

        for (i=1; i<k; i++) in_perm[ a[i] ] = TRUE;  // we are finding candidates that for a_k, a_k+1, ... a_n
                                                     // when k == 1, all candidates are valid because we haven't selected any yet
                                                     // when k == 2, all candidates except a_1 are valid
                                                     // when k == n, all candidates except a_1 .. a_n-1 are valid

        *ncandidates = 0;
        for (i=1; i<=n; i++)
                if (in_perm[i] == FALSE) {
                        c[ *ncandidates] = i;
                        *ncandidates = *ncandidates + 1;
                }

}
 

void process_solution(int a[], int k, int input) {

   int i;                       /* counter */

   for (i=1; i<=k; i++) 
      printf(" %d",a[i]);

   printf("\n");

}
 





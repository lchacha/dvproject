/* 

  travellingSalesmanApplication.cpp - application file for the solution of the travelling saleman problem by exhaustive search using backtracking

  04-630 Data Structures and Algorithms for Engineers Assignment No. 7

  See travellingSalesman.h for details of the problem

  David Vernon
  24 April 2017

*/
 
#include "travellingSalesman.h"

int main() {

   struct record_type record[NUMBER_OF_STOPS];

   int i, j, k;                                      // general purpose counters
   int n;                                            // number of customer drop-off locations
   int number_of_test_cases;                         //
   char stop_name[STRING_SIZE];                      // general purpose string
   int distances[NUMBER_OF_STOPS][NUMBER_OF_STOPS];  // distances between location i (row) and location j (column)
   int a[NMAX];

   int debug = TRUE;                                 // flag: if TRUE print information to assist with debugging

   FILE *fp_in, *fp_out;                             // input and output file pointers


   /* open input and output files */

   if ((fp_in = fopen("../data/input.txt","r")) == 0) {
	   printf("Error can't open input input.txt\n");
      exit(0);
   }

   if ((fp_out = fopen("../data/output.txt","w")) == 0) {
	   printf("Error can't open output output.txt\n");
      exit(0);
   }

   /* read the number of test cases */

   fscanf(fp_in, "%d", &number_of_test_cases);
   fgetc(fp_in); // move past end of line for subsequent fgets; fgets fails unless we do this

   if (debug) printf ("%d\n", number_of_test_cases);


   /* main processing loop, one iteration for each test case */

   for (k=0; k<number_of_test_cases; k++) {

      /* read the data for each test case  */
      /* --------------------------------  */
      
      /* number of customer drop-off locations */

      fscanf(fp_in, "%d", &n);
      fgetc(fp_in);  // move past end of line for subsequent fgets

      if (debug) printf ("%d\n",n);

      /* get the location names and the restaurant name */

      for (i = 0; i < n+1; i++) {

         fgets(stop_name, STRING_SIZE, fp_in);
         remove_new_line(stop_name);

         record[i].key = i+1;
         strcpy(record[i].string,stop_name);

         if (debug) printf ("%s\n",record[i].string);
      }

      /* get the matrix of distances */

      for (i = 0; i < n+1; i++) {
         for (j = 0; j < n+1; j++) {
            fscanf(fp_in, "%d", &(distances[i][j]));
         }
      }

      if (debug) {
         for (i = 0; i < n+1; i++) {
            for (j = 0; j < n+1; j++) {
               printf("%3d ", distances[i][j]);
            }
            printf("\n");
         }
      }


      /* main processing begins here */
      /* --------------------------- */

      /* use backtracking to generate the permutations and store the permutation giving the lowest total distance */

      backtrack(a,0,n);  // this is the original call and will just print the permutations to the terminal
                         // it must be modified so that the distances array is passed as an argument
                         // so that the permutation giving the minimum distance can be identified
                         // it may also be useful to have it return the permutation yielding the minimum distance
                         // Remember: these permutations are for numbers 1 to n, not 0 to n-1
                         // this matters when you use the permutation elements to access the distance array

      if (debug) getchar();

   }

   fclose(fp_in);
   fclose(fp_out);                                                         
}
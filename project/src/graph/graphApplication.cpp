/* 
 
  Application file 

  Driver to demonstrate some of the functionality of the graph adjacency list implementation.

  David Vernon
  20 March 2017

*/
 
#include "graph.h"
#include <iostream>
 
using namespace std;
int main() {

   bool debug = false;

   graph g;
   bool directed = true;
   k_distribution head;
 //  read_graph(&g, directed);       // build the graph interactivel

   read_graph_from_file(&g, directed);
   
   print_graph(&g);                // print the graph to screen

   average_degree(&g, directed);		// Calculated the average degree of the graph

   nth_moment(&g, directed, 2);         //Calculated the nth moment specified by the third variable example 2nd momemnt in this case

   g_standard_deviation(&g);    

   loglog_degree_distribution(&g, &head);

   cout << head.degree << "IN application file -- caller file " << endl;
   k_distribution *next = &head;
   cout << next->next << "\t" << head.degree << "\t" << head.pk << endl;

   cout << "Printing elements in queue" << endl;
   while(next)
   {
	   cout << next->degree << "\t" << next->pk << endl;
	   next = next->next;
   }
  log_log_exponent(&g, &head);
  // find_path(&g, 1, g.nvertices);  // find the path from vertex 1 to vertex n, where n is the last vertex in the graph
  
   prompt_and_exit(0);
}

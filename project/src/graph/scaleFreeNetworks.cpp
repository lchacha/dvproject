
#include "graph.h"
#include <fstream>
#include <assert.h>
#include <iostream>
#include <string>
#include <cmath>

using namespace std;

 /* create a log-log distriution Data structure */
k_distribution *last = NULL;
/*
  This method computes the log-log value of the degree distribution
*/
void  loglog_degree_distribution(graph *g, k_distribution *head)
{

	cout << "in loglog"<< g->nvertices << endl;
	int *loglogArray=NULL;

	loglogArray = new int[g->nvertices];

	double i_k;

	/* for each node find the log probability and the degree-k */
	for( int i = 1; i <= g->nvertices ; i++)
	{
		i_k = nth_degree(g, i);
		loglogArray[i-1] = i_k;
		cout << endl << i << "\t" << loglogArray[i-1];

	}
	/* Now that we have the degreed of each of the nodes
	This section finds the degree degree distriution of the graph*/
	
	insertionSort(loglogArray, g->nvertices);

	/* Now find how many of each value is in the sorte array */
	for(int j=0; j < g->nvertices; j++)
	{
		cout << "print the array   " << loglogArray[j] << endl;
	}
	
	int key = loglogArray[0];
	int counter = 1;
	k_distribution *new_node;
	int str;
	char buffer [1000];

	for( int y=1; y < g->nvertices; y++)
	{
	
		if(loglogArray[y] == key)
		{
			counter++;
		}else{
			/* store the new tally to the queue and update the key */
         
			if(last == NULL)
			{
				new_node =  new (struct k_distribution);
				head->degree = key;
				head->pk =  (float)counter/float(g->nvertices);
				head->next = new_node;
				last = head;
				
			}else{
			new_node =  new (struct k_distribution);
			new_node->degree = key;
			new_node->pk = (float)counter/float(g->nvertices);
			

			/* there are elements that exist */
			last->next = new_node;
		    last = new_node;
			}
			cout << "NEW element degree          " << last->degree << endl;
			cout << "NEW element prob          " << last->pk << endl;
			str = sprintf(buffer, "%d %0.4f\n", last->degree,  last->pk);
			write_to_File( buffer);
			/* key = loglogArray[y] */
			 key = loglogArray[y];
			 counter = 1;
			 
		}
		
		// break;
	}
	new_node =  new (struct k_distribution);
	new_node->degree = key;
	new_node->pk = (float)counter/float(g->nvertices);
			
	/* there are elements that exist */
	last->next = new_node;
    last = new_node;
	
}

double log_log_exponent(graph *g, k_distribution *head )
{
	/* Loop though the linked -list and find the parameters to calculate the exponent*/
	
	double logxSum=0.0;
	double logySum=0.0;
	double logxySum=0.0;
	double logx2Sum=0.0;
	double logX=0.0;
	double logY=0.0;
	double average_k;
	int nElems = 0; 


	k_distribution *counter = head;
	average_k = average_degree(g, true);
	while(counter)
	{
		// Takes care of the undefined log(zero) 
		if(counter->degree == 0)
		{
			counter = counter->next;
			continue;
		}
		logX = log(average_k) - log(double(counter->degree));
		logY = log(double(counter->pk));
		cout << logX << "\t" << logY << endl;
		/* Calculate the parameters */
		logxSum += logX;
		logySum += logY;
		logxySum += (logX*logY);
		logx2Sum +=  pow(logX,2);
		nElems++;
		counter = counter->next;
		//cout  <<logX <<"\t" <<  logxSum << "\t" <<counter->pk <<"\t" <<logY << "\t"<< logySum << "\t" << logxySum<<"\t" << logx2Sum<<  endl;
	}
		
		cout << "Number of nodes in distribution:    "<< nElems << endl;
		/* Calculate the exponent*/
		double exp = (logySum * logxSum - nElems * logxySum)/(pow(logxSum,2) - nElems * logx2Sum) ;

		cout << "exponent:    "<< exp << endl;
		return exp;
		
}

void insertionSort(int * arr, int n)
{
   int i, key, j;
   for (i = 1; i < n; i++)
   {
       key = arr[i];
       j = i-1;
 
       /* Move elements of arr[0..i-1], that are
          greater than key, to one position ahead
          of their current position */
       while (j >= 0 && arr[j] > key)
       {
		   cout << arr[j+1] << "\t" << arr[j] << endl;
           arr[j+1] = arr[j];
		   cout << arr[j+1] << "\t" << arr[j] << endl;
           j = j-1;
		   cout <<endl;
       }
       arr[j+1] = key;
   }
}
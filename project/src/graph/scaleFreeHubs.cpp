
#include "graph.h"
#include <fstream>
#include <assert.h>
#include <iostream>
#include <string>
#include <cmath>
#include <math.h>

//using namespace std;

 /* create a log-log distriution Data structure */
k_distribution *last = NULL;
int k_min = 1;
/*
log-log lambda is based on the least squares regression algorithm
*/
double log_log_lambda(graph *g, k_distribution *head )
{
	/* Loop though the linked -list and find the parameters to calculate the exponent*/
	
	double logxSum=0.0;
	double logySum=0.0;
	double logxySum=0.0;
	double logx2Sum=0.0;
	double logX=0.0;
	double logY=0.0;
	double average_k;
	int nElems = 0; 


	k_distribution *counter = head;
	//average_k = average_degree(g, true);
	while(counter)
	{
		// Takes care of the undefined log(zero) 
		if(counter->degree == 0)
		{
			counter = counter->next;
			continue;
		}
		logX =log(exp(double(counter->degree - k_min)) );
		logY = log(double(counter->pk));
		//std::cout << logX << "\t" << logY << endl;
		/* Calculate the parameters */
		logxSum += logX;
		logySum += logY;
		logxySum += (logX*logY);
		logx2Sum +=  pow(logX,2);
		nElems++;
		counter = counter->next;
		//cout  <<logX <<"\t" <<  logxSum << "\t" <<counter->pk <<"\t" <<logY << "\t"<< logySum << "\t" << logxySum<<"\t" << logx2Sum<<  endl;
	}
		//cout<<here;
		//cout << "Number of nodes in distribution:    "<< nElems << endl;
		/* Calculate the exponent*/
		double exp = ((logySum * logxSum) - (nElems * logxySum))/(pow(logxSum,2) - (nElems * logx2Sum)) ;

		printf( "exponent:   %f ", sqrt(exp) );
		return sqrt(exp);
		
}
/* Function computes the k_max of a network hub*/
double network_max_hub(graph *g, k_distribution *head )
{
	double k_max = k_min + (log(double(g->nvertices))/log_log_lambda(g, head);
}
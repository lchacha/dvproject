/* 

  Implementation file
 
  Graph - adjacency list implementation based on code by Steven Skiena:

  Steven S. Skiena, "The Algorithm Design Manual", 2nd Edition, Springer, 2008.

  David Vernon
  19 March 2017

*/
 
#include "graph.h"
#include <fstream>
#include <assert.h>
#include <iostream>
#include <string>
#include <cmath>

using namespace std;
/* Breadth-First Search data structures */

bool processed[MAXV+1];   /* which vertices have been processed */
bool discovered[MAXV+1];  /* which vertices have been found */
int  parent[MAXV+1];      /* discovery relation */

bool debug = true;
 float average_k;


/* Initialize graph  */

void initialize_graph(graph *g, bool directed){

   int i;                          /* counter */

   g -> nvertices = 0;
   g -> nedges = 0;
   g -> directed = directed;

   for (i=1; i<=MAXV; i++) 
      g->degree[i] = 0;

   for (i=1; i<=MAXV; i++) 
      g->edges[i] = NULL; 
}

/* Initialize graph from data in a file                             */
/* return false when the number of vertices is zero; true otherwise */

bool read_graph_from_file(graph *g, bool directed){
	//int i; /* counter */
	int m;  /* number of edges*/
	int x,y;   /* Vertices in edges (x.y) */
	int w;     /* Weight of the edge */

	initialize_graph(g, directed);
	
	// declare stream variable
	std::ifstream fin("../data/network1.txt", ifstream:: in);
	string line;
	// Read the number of vertices and	edges from file 
	fin >> g->nvertices >> m;

	if(fin.is_open())
		{
			while ( getline(fin, line))
				{
		
					fin >> x >> y >> w;

					// Insert the node into the graph
					insert_edge(g,x,y,directed, w);
				}
	}else
	{
		cout << "Unable to open file";
	}

	// assert(!fin.fail());

	// get first row in file 

	// printf("%d", (g->nvertices));
	//cout << i ;
	//cout << m; 

	return(true);

}
bool read_graph(graph *g, bool directed) { 

   int i;    /* counter                */
   int m;    /* number of edges        */
   int x, y; /* vertices in edge (x,y) */
   int w;    /* weight on edge (x,y)   */

   initialize_graph(g, directed);

   printf("Enter the number of vertices >> ");
   scanf("%d",&(g->nvertices));
   printf("Enter the number of edges    >> ");
   scanf("%d",&m);

   //if (debug) printf("%d %d\n",g->nvertices,m);

   if (g->nvertices != 0) {
      for (i=1; i<=m; i++) {
         printf("Enter edge vertices, x and y, and weight, w >> ");
         scanf("%d %d %d",&x,&y,&w);
         //if (debug) printf("%d %d %d\n",x,y,w);
         insert_edge(g,x,y,directed, w);
      } 
      return(true);
   }
   else {
      return(false);
   }

 
}  

/* insert edge in a graphs */

void insert_edge(graph *g, int x, int y, bool directed, int w) {

   edgenode *p;                  /* temporary pointer */
      
   p = (EDGENODE_PTR) malloc(sizeof(edgenode)); /* allocate edgenode storage */
     //^^^^^^^^^^^^^^ ADDED CAST. DV 7/11/2014

   p->weight = w;
   p->y = y;
   p->next = g->edges[x];

   g->edges[x] = p;              /* insert at head of list        */

   g->degree[x] ++;

   if (directed == false)        /* NB: if undirected add         */
      insert_edge(g,y,x,true,w);   /* the reverse edge recursively  */  
   else                          /* but directed TRUE so we do it */
      g->nedges ++;              /* only once                     */
}

/* Print a graph                                                    */

void print_graph(graph *g) {
        
   int i;                             /* counter           */
   edgenode *p;                       /* temporary pointer */

   printf("Graph adjacency list:\n");

   for (i=1; i<=g->nvertices; i++) {
      printf("%d: ",i);
      p = g->edges[i];
      while (p != NULL) {

         printf(" %d-%d", p->y, p->weight);

         p = p->next;
      }
      printf("\n");
   }
}

/* Each vertex is initialized as undiscovered:                      */

void initialize_search(graph *g){
        
   int i;                          /* counter */
   
   for (i=1; i<=g->nvertices; i++) {
      processed[i] = discovered[i] = FALSE;
      parent[i] = -1;
   } 
}

/* Once a vertex is discovered, it is placed on a queue.           */
/* Since we process these vertices in first-in, first-out order,   */
/* the oldest vertices are expanded first, which are exactly those */
/* closest to the root                                             */

void bfs(graph *g, int start)
{
   queue q;                  /* queue of vertices to visit */
   int v;                    /* current vertex             */
   int y;                    /* successor vertex           */
   edgenode *p;              /* temporary pointer          */

   init_queue(&q);
   enqueue(&q,start);
   discovered[start] = TRUE;
   while (empty_queue(&q) == FALSE) {
      v = dequeue(&q);
      process_vertex_early(v);
      processed[v] = TRUE;
      p = g->edges[v];

      while (p != NULL) {  
         
         y = p->y;
         if ((processed[y] == FALSE) || g->directed)
            process_edge(v,y);
         if (discovered[y] == FALSE) {
            enqueue(&q,y);
            discovered[y] = TRUE;
            parent[y] = v;
         }

         p = p->next; 
      }
      process_vertex_late(v);
   }
}

/* The exact behaviour of bfs depends on the functions             */
/*    process vertex early()                                       */
/*    process vertex late()                                        */
/*    process edge()                                               */
/* These functions allow us to customize what the traversal does   */
/* as it makes its official visit to each edge and each vertex.    */
/* Here, e.g., we will do all of vertex processing on entry        */
/* (to print each vertex and edge exactly once)                    */
/* so process vertex late() returns without action                 */

void process_vertex_late(int v) {
}

void process_vertex_early(int v){
   //printf("processed vertex %d\n",v);
}

void process_edge(int x, int y) {
   //printf("processed edge (%d,%d)\n",x,y);
}

/* this version just counts the number of edges                     */
/*
void process_edge(int x, int y) {
   nedges = nedges + 1;
}
*/


/* adapted from original to return true if it is possible to reach the end from the start */

bool find_path(int start, int end, int parents[]) {
   
   bool is_path;

   if (end == -1) {
      is_path = false; // some vertex on the path back from the end has no parent (not counting start)
      if (debug) printf("\nis_path is false: ");
   }
   else if ((start == end)) {
       if (debug) printf("\n%d",start);
       is_path = true; // we have reached the start vertex
   }
   else {
      is_path = find_path(start,parents[end],parents);
      if (debug) printf(" %d",end);
   }
   return(is_path); 
}
 
/* DV abstract version that hides implementation */

bool find_path(graph *g, int start, int end) {
   bool is_path;

   if (debug) printf("Path from %d to %d:",start,end);

   if ((start < 1) || (start > g->nvertices)) {
      if (debug) 
         printf("Invalid start vertex\n");
      is_path = false;
   }
   else if ((end < 1) || (end > g->nvertices)) {
      if (debug) 
         printf("Invalid end vertex\n");
      is_path = false;
   }
   else {
      initialize_search(g);
      bfs(g, start);
      is_path = find_path(start, end, parent);
      if (debug) printf("\n");
   }
   return(is_path);
}



/*	queue

	Implementation of a FIFO queue abstract data type.

	by: Steven Skiena
	begun: March 27, 2002
*/


/*
Copyright 2003 by Steven S. Skiena; all rights reserved. 

Permission is granted for use in non-commerical applications
provided this copyright notice remains intact and unchanged.

This program appears in my book:

"Programming Challenges: The Programming Contest Training Manual"
by Steven Skiena and Miguel Revilla, Springer-Verlag, New York 2003.

See our website www.programming-challenges.com for additional information.

This book can be ordered from Amazon.com at

http://www.amazon.com/exec/obidos/ASIN/0387001638/thealgorithmrepo/

*/
 
void init_queue(queue *q)
{
        q->first = 0;
        q->last = QUEUESIZE-1;
        q->count = 0;
}

void enqueue(queue *q, item_type x)
{
        if (q->count >= QUEUESIZE)
		printf("Warning: queue overflow enqueue x=%d\n",x);
        else {
                q->last = (q->last+1) % QUEUESIZE;
                q->q[ q->last ] = x;    
                q->count = q->count + 1;
        }
}

item_type dequeue(queue *q)
{
        item_type x;

        if (q->count <= 0) printf("Warning: empty queue dequeue.\n");
        else {
                x = q->q[ q->first ];
                q->first = (q->first+1) % QUEUESIZE;
                q->count = q->count - 1;
        }

        return(x);
}

item_type headq(queue *q)
{
	return( q->q[ q->first ] );
}

int empty_queue(queue *q)
{
        if (q->count <= 0) return (TRUE);
        else return (FALSE);
}

void print_queue(queue *q)
{
        int i;

        i=q->first; 
        
        while (i != q->last) {
                printf("%d ",q->q[i]);
                i = (i+1) % QUEUESIZE;
        }

        printf("%2d ",q->q[i]);
        printf("\n");
}


void prompt_and_exit(int status) {
   printf("Press any key to continue and close terminal\n");
   getchar();
   getchar();
   exit(status);
}

void write_to_File( char buffer[1000]){
	string file  =  "../data/network1Output.txt";
	ofstream fout(file,  std::ios_base::app);
	fout << buffer;
	fout.close();


}
/* Functions beyound here calculate the network characteristics */

/* Calculates the average degree of the graph g
In  directed graph kin = kout = L(totatl number of links)

This makes the average degree = number of edges/ number of vertices

In  a undirected network  average degree = 2L/N */
double  average_degree(graph *g, bool directed)
{
	char buffer [1000];
	if(directed)  /* If the graph is directed*/
	{
		// Print the average degree to file:
		average_k =  (float)(g->nedges)/(float)(g->nvertices);
		int str = sprintf(buffer, "%s:%0.4f\n", "Average Degree",average_k);
		write_to_File( buffer);

	}else /* if the graph is undirected*/
	{
		average_k =  (2.0 * (float)(g->nedges))/(float)(g->nvertices);
		int str = sprintf(buffer, "%s:%0.4f\n", "Average Degree", average_k);
		write_to_File( buffer);
	}
	return average_k;
}

void nth_moment(graph *g, bool directed, int moment)
{
	int kdegree;
	int kd_sum = 0;

	// string holder
	char buffer [1000];
	// loop though each node and get the degree of each node
	for(int i=1; i<=g->nvertices; i++) {
		kdegree = nth_degree(g, i);
		//cout << kdegree<< "\t" << pow((double)kdegree,moment)<<  endl;
		kd_sum = kd_sum + pow((double)kdegree,moment);
		//cout << "node k_sum:       " << kd_sum<<endl;
	}
	//cout << "k_sum:     " <<kd_sum << endl;
	int str = sprintf(buffer, "%d%s:%0.4f\n", moment, "th moment", (float)kd_sum/(float)(g->nvertices));
	write_to_File( buffer);
}

int nth_degree(graph *g, int vertexNo)
{
	  edgenode *p;					// temporary node
	  int k = 0;									// degree counter
     // printf("%d: ",vertexNo);
      p = g->edges[vertexNo];
      while (p != NULL) {
		  k ++;
        // printf(" %d-%d", p->y, p->weight);

         p = p->next;
      }
	  return(k);
}

float g_variance(graph *g)
{
	int kdegree;
	int kn_diff = 0;
	int kn_sum = 0;

	// string holder
	char buffer [1000];

	// loop though each node and get the degree of each node
	for(int i=1; i<=g->nvertices; i++) {
		kdegree = nth_degree(g, i);
		kn_sum =  kn_sum + pow(((double)kdegree - average_k), 2);
	}
	//cout << "k_sum:     " <<kd_sum << endl;
	float variance =  (float)kn_sum/(float)(g->nvertices);
	int str = sprintf(buffer, "%s:%0.4f\n","variance", variance);
	write_to_File( buffer);
	return( variance);
}

float g_standard_deviation(graph *g)
{
	char buffer [1000];
	float variance = g_variance(g);
	cout << variance << endl;
	double  std = sqrt(variance);
	cout << std << endl;
	// write to file
	int str = sprintf(buffer, "%s:%0.8f\n","Standard Deviation", std);
	write_to_File( buffer);

	// return value
	return( variance);
}


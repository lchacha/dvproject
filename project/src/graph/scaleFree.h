/* 
  Interface file

  Graph - adjacency list implementation based on code by Steven Skiena:

  Steven S. Skiena, "The Algorithm Design Manual", 2nd Edition, Springer, 2008.

  Lenah Chacha
  August 2017

*/
 
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include <ctype.h>
#include <fstream>
#include "graph.h"

using namespace std;

double average_degree(graph *g, bool directed);

void write_to_File(char buffer[1000]);

void nth_moment(graph *g, bool directed, int moment);

int nth_degree(graph *g, int vertexNo);

float g_variance(graph *g);

float g_standard_deviation(graph *g);

void loglog_degree_distribution(graph *g, k_distribution *head);

void insertionSort(int * arr, int n);

double log_log_exponent(graph *g, k_distribution *head );
 
/* 

  Implementation file
 
  Graph - adjacency list implementation based on code by Steven Skiena:
  Grapg - analysis ased on Barabasi

  Steven S. Skiena, "The Algorithm Design Manual", 2nd Edition, Springer, 2008.

 Lenah Chacha
  19 July 2017

*/

#include "graph.h"
#include <fstream>
#include <assert.h>
#include <iostream>
#include <string>
#include <cmath>

using namespace std;

/* Functions beyound here calculate the network characteristics */

/* Calculates the average degree of the graph g
In  directed graph kin = kout = L(totatl number of links)

This makes the average degree = number of edges/ number of vertices

In  a undirected network  average degree = 2L/N */
float (loglog_degree_distribution(graph *g)
{

	cout << "in loglog"<< g->nvertices << endl;
	int *loglogArray=NULL;

	loglogArray = new int[g->nvertices];

	double i_k;

	/* for each node find the log probability and the degree-k */
	for( int i = 1; i <= g->nvertices ; i++)
	{
		i_k = nth_degree(g, i);
		loglogArray[i-1] = i_k;
		cout << endl << i << "\t" << loglogArray[i-1];

	}
	/* Now that we have the degreed of each of the nodes
	This section finds the degree degree distriution of the graph*/
	
	insertionSort(loglogArray, g->nvertices);

	/* Now find how many of each value is in the sorte array */
	int key = loglogArray[0];
	int counter = 1;
	for( int y=1; y < g->nvertices; y++)
	{
		if(loglogArray[y] == key)
		{
			counter++;
		}
		else{
			/* store the new tally to the queue and update the key */
			k_distribution new_node;
			new_node.degree = key;
			new_node.pk = (counter/g->nvertices);

			/* key = loglogArray[y] */
			 key = loglogArray[y];
			 counter = 1;
		}
	}
	/* The degree distribution is stored in a queue*/

	cout << endl << "sorted      " <<   sizeof(loglogArray) << endl;
	for( int i = 1; i <= g->nvertices ; i++)
	{
		cout << endl << i << "\t" << loglogArray[i-1];

	}
	cout << endl;
	delete [] loglogArray;
	return(0);
	
}

void insertionSort(int * arr, int n)
{
   int i, key, j;
   for (i = 1; i < n; i++)
   {
       key = arr[i];
       j = i-1;
 
       /* Move elements of arr[0..i-1], that are
          greater than key, to one position ahead
          of their current position */
       while (j >= 0 && arr[j] > key)
       {
		   cout << arr[j+1] << "\t" << arr[j] << endl;
           arr[j+1] = arr[j];
		   cout << arr[j+1] << "\t" << arr[j] << endl;
           j = j-1;
		   cout <<endl;
       }
       arr[j+1] = key;
   }
}